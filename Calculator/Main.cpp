//C++ Project 2 Simple Calculator
//Michelle Morris

#include <iostream>
#include <conio.h>

using namespace std;

float Add(float num1, float num2)
{
	return (num1 + num2); 
}

float Subtract(float num1, float num2)
{
	return (num1 - num2);
}

float Multiply(float num1, float num2)
{
	return (num1 * num2);
}
bool Divide(float num1, float num2, float &answer)
{
	if (num2 == 0)
	{
		cout << "Cannot divide by 0, answer will remain 0 \n";
		return false;
	}

	else
	{
		answer = (num1 / num2);
		return true;
	}
}
int main()
{
	float num1;
	float num2;
	char mathsymbol;
	float answer; 
	char input = 'q';

	do
	{
		answer = 0;

		cout << "Enter an equation using two positive numbers and one of the following characters, +, -, *, or / \n";

		cout << "Enter a positive number \n";

		cin >> num1;

		cout << "Enter a math symbol like +, -, *, / \n";

		cin >> mathsymbol;

		cout << "Enter another positive number \n";

		cin >> num2;

		switch (mathsymbol)
		{
		case '+': answer = Add(num1, num2); break;
		case '-': answer = Subtract(num1, num2); break;
		case '*': answer = Multiply(num1, num2); break;
		case  '/': Divide(num1, num2, answer); break;
		}

		cout << "The answer is " << answer << "\n";
		cout << "Enter 'q' to quit or any key to continue: ";
		cin >> input;

	} while (input != 'q' && input != 'Q');

	_getch();
	return 0;
}

